from .keys import PEXEL_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_image(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"

    headers = {"Authorization": PEXEL_API_KEY}

    resp = requests.get(url, headers=headers)
    data = resp.json()
    print(data)

    return data["photos"][0]["src"]["original"]


def get_weather(city, state):
    city = city
    state = state

    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=5&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    lat, lon = resp.json()[0]["lat"], resp.json()[0]["lon"]

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    description = resp.json()["weather"][0]["description"]
    temp = resp.json()["main"]["temp"]
    return {"description": description, "temp": temp}
